#include <ctype.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern char label[100];
extern int pos;

typedef enum {
    TK_RESERVED,
    TK_IDENT,
    TK_TYPE,
    TK_NUM,
    TK_EOF,
} TokenKind;

typedef struct Token Token;

extern char *user_input;

extern Token *token;

struct Token {
    TokenKind kind;
    Token *next;
    int val;
    char *str;
    int len;
};

typedef enum {
    ND_ADD,
    ND_SUB,
    ND_MUL,
    ND_DIV,
    ND_RETURN,
    ND_ASSIGN,
    ND_LVAR,
    ND_FUNCPROT,
    ND_FUNCCALL,
    ND_FUNCDECL,
    ND_EQ,
    ND_NE,
    ND_LT,
    ND_LE,
    ND_NUM,
    ND_BLOCK,
    ND_IF,
    ND_ELSE,
    ND_NOELSE,
    ND_WHILE,
    ND_FOR,
    ND_FORTEST,
    ND_CONTINUE,
    ND_BREAK,
    ND_EMPTY,
    ND_ADDR,
    ND_DEREF,
    ND_STMT,
} NodeKind;

typedef struct Node Node;

extern Node *code[100];

typedef struct NodeVec NodeVec;

typedef struct Funcs Funcs;

struct Node {
    NodeKind kind;
    Node *lhs;
    Node *rhs;
    NodeVec *block;
    int val;
    int offset;
    Funcs *fptr;
};


struct NodeVec {
    Node *node;
    NodeVec *next;
};

typedef struct LVar LVar;

struct LVar {
    LVar *next;
    char *name;
    int len;
    int offset;
};

struct Funcs {
    Funcs *next;
    char *name;
    int len;
    NodeVec *args;
    LVar *locals;
};

Funcs *funcs;

extern Funcs *funcs;

void error(char *fmt, ...);
void error_at(char *loc, char *fmt, ...);
bool consume(char *op);
void expect(char *op);
int expect_number();
bool at_eof();
Token *new_token(TokenKind kind, Token *cur, char *str, int len);
bool startswith(char *p, char *q);
Token *tokenize(char *p);
Node *new_node(NodeKind kind, Node *lhs, Node *rhs);
Node *new_node_num(int val);
Token *consume_ident();
Node *expr();
Node *primary();
Node *unary();
Node *mul();
Node *add();
Node *relational();
Node *equality();
Node *assign();
Node *stmt();
void program();
void gen_lval(Node *node);
void gen(Node *node);
