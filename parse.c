#include "9cc.h"

char *user_input;

Token *token;

LVar *locals;

void error(char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    exit(1);
}

void error_at(char *loc, char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);

    int pos = loc - user_input;
    fprintf(stderr, "%s\n", user_input);
    fprintf(stderr, "%*s", pos, " ");
    fprintf(stderr, "^ ");
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    exit(1);
}

bool peek(char *op) {
    if (strlen(op) != token->len ||
        memcmp(token->str, op, token->len))
    {
        return false;
    }
    return true;
}

bool consume(char *op) {
    if (token->kind != TK_RESERVED ||
        strlen(op) != token->len ||
        memcmp(token->str, op, token->len))
    {
        return false;
    }
    token = token->next;
    return true;
}

void expect(char *op) {
    if (token->kind != TK_RESERVED ||
        strlen(op) != token->len ||
        memcmp(token->str, op, token->len))
    {
        error_at(token->str, "'%s'ではありません", op);
    }
    token = token->next;
}

int expect_number() {
    if (token->kind != TK_NUM) {
        error_at(token->str, "数ではありません");
    }
    int val = token->val;
    token = token->next;
    return val;
}

bool at_eof() {
    return token->kind == TK_EOF;
}

Token *new_token(TokenKind kind, Token *cur, char *str, int len) {
    Token *tok = calloc(1, sizeof(Token));
    tok->kind = kind;
    tok->str = str;
    cur->next = tok;
    tok->len = len;
    return tok;
}

bool is_alnum(char c) {
    return ('a' <= c && c <= 'z') ||
           ('A' <= c && c <= 'Z') ||
           ('0' <= c && c <= '9') ||
           (c == '_');
}

bool startswith(char *p, char *q) {
    return memcmp(p, q, strlen(q)) == 0;
}

Token *tokenize(char *p) {
    Token head;
    head.next = NULL;
    Token *cur = &head;

    char *types[] = {"int", NULL};
    char *keywords[]  = {"return", "if", "else", "while", "for", "continue", "break", NULL};
    char *reserveds[] = {"<=", ">=", "==", "!=", "+=", "-=", "*=", "/=", "++", "--", "+", "-",
                         "*", "/", "(", ")", "<", ">", "=", ";", "{", "}", ",", "&", NULL};
    while (*p) {
        if (isspace(*p)) {
            ++p;
            continue;
        }

        if (isdigit(*p)) {
            cur = new_token(TK_NUM, cur, p, 0);
            cur->val = strtol(p, &p, 10);
            continue;
        }

        for (int i = 0; types[i] != NULL; ++i) {
            if (startswith(p, types[i]) && !is_alnum(p[strlen(types[i])])) {
                cur = new_token(TK_TYPE, cur, p, strlen(types[i]));
                p += strlen(types[i]);
                goto tail;
            }
        }

        for (int i = 0; keywords[i] != NULL; ++i) {
            if (startswith(p, keywords[i]) && !is_alnum(p[strlen(keywords[i])])) {
                cur = new_token(TK_RESERVED, cur, p, strlen(keywords[i]));
                p += strlen(keywords[i]);
                goto tail;
            }
        }

        for (int i = 0; reserveds[i] != NULL; ++i) {
            if (startswith(p, reserveds[i])) {
                cur = new_token(TK_RESERVED, cur, p, strlen(reserveds[i]));
                p += strlen(reserveds[i]);
                goto tail;
            }
        }

        if (('a' <= *p && *p <= 'z') || ('A' <= *p && *p <= 'Z') || *p == '_') {
            int i = 0;
            while (is_alnum(*p)) {
                ++p;
                ++i;
            }
            cur = new_token(TK_IDENT, cur, p - i, i);
            continue;
        }

        error_at(p, "トークナイズできません");
    tail:;
    }

    new_token(TK_EOF, cur, p, 0);
    return head.next;
}

Node *new_post_increment(Node *node, NodeKind type) {
    Node *tmp = malloc(sizeof(Node));
    Node *tmp2 = malloc(sizeof(Node));
    tmp = memcpy(tmp, node, sizeof(Node));
    tmp2 = memcpy(tmp2, node, sizeof(Node));
    tmp2 = new_node(type, tmp2, new_node_num(1));
    tmp = new_node(ND_ASSIGN, tmp, tmp2);
    tmp = new_node(ND_STMT, tmp, NULL);
    node->kind = ND_BLOCK;
    node->block = malloc(sizeof(NodeVec));
    node->block->node = new_node(ND_LVAR, NULL, NULL);
    node->block->node->offset = node->offset;
    node->block->next = calloc(1, sizeof(NodeVec));
    node->block->next->node = tmp;
    node->block->next->next = calloc(1, sizeof(NodeVec));
    return node;
}

Node *new_node(NodeKind kind, Node *lhs, Node *rhs) {
    Node *node = calloc(1, sizeof(Node));
    node->kind = kind;
    node->lhs = lhs;
    node->rhs = rhs;
    return node;
}

Node *new_node_num(int val) {
    Node *node = calloc(1, sizeof(Node));
    node->kind = ND_NUM;
    node->val = val;
    return node;
}

Token *consume_type() {
    if (token->kind != TK_TYPE) {
        return NULL;
    }
    Token *tok = token;
    token = token->next;
    return tok;
}

Token *consume_ident() {
    if (token->kind != TK_IDENT) {
        return NULL;
    }
    Token *tok = token;
    token = token->next;
    return tok;
}

LVar *find_lvar(Token *tok) {
    for (LVar *var = locals; var; var = var->next) {
        if (var->len == tok->len &&
            !memcmp(tok->str, var->name, var->len))
        {
            return var;
        }
    }
    return NULL;
}

Funcs *find_func(Token *tok) {
    for (Funcs *var = funcs; var; var = var->next) {
        if (var->len == tok->len &&
            !memcmp(tok->str, var->name, var->len))
        {
            return var;
        }
    }
    return NULL;
}

Node *expr();

Node *func_declaration(Token *tok, NodeKind kind) {
    Node *node = calloc(1, sizeof(Node));
    node->fptr = calloc(1, sizeof(Funcs));
    node->fptr->args = calloc(1, sizeof(NodeVec));
    NodeVec *args = node->fptr->args;

    int count = 0;
    while (!consume(")")) {
        ++count;
        args->node = expr();
        args->next = calloc(1, sizeof(NodeVec));
        args = args->next;

        if (consume(",") && peek(")")) {
            error_at(token->str, ",の後に)が存在します");
        }
    }

    args = node->fptr->args;
    node->kind = kind;

    Funcs *cur_func = find_func(tok);
    if (cur_func) {
        if (kind == ND_FUNCDECL) {
            error_at(tok->str, "関数の宣言が複数存在します");
        }
        int check = 0;
        node->fptr = calloc(1, sizeof(Funcs));
        NodeVec *i = cur_func->args;
        while (i->next != NULL) {
            ++check;
            i = i->next;
        }
        if (check != count) {
            for (int i = 0; i < cur_func->len; ++i) {
                fprintf(stderr, "%c", cur_func->name[i]);
            }
            error("関数の引数の個数が異なります: 宣言: %d, 呼び出し: %d", check, count);
        }
        node->fptr = malloc(sizeof(Funcs));
        memcpy(node->fptr, cur_func, sizeof(Funcs));
        node->fptr->args = args;
    } else {
        cur_func = calloc(1, sizeof(Funcs));
        cur_func->next = funcs;
        cur_func->name = tok->str;
        cur_func->len = tok->len;
        cur_func->args = args;
        node->fptr = cur_func;
        funcs = cur_func;
    }
    return node;
}

Node *primary() {
    if (consume("(")) {
        Node *node = expr();
        expect(")");
        return node;
    }
    Token *type = consume_type();
    Token *tok = consume_ident();
    if (tok) {
        Node *node = calloc(1, sizeof(Node));

        if (consume("(")) {
            if (type) {
                node = func_declaration(tok, ND_FUNCPROT);
            } else {
                node = func_declaration(tok, ND_FUNCCALL);
            }
            return node;
        }
        node->kind = ND_LVAR;

        LVar *lvar = find_lvar(tok);
        if (lvar) {
            if (type) {
                error_at(token->str, "既に同名の変数が宣言されています");
            }
            node->offset = lvar->offset;
            if (consume("++")) {
                node = new_post_increment(node, ND_ADD);
            } else if (consume("--")) {
                node = new_post_increment(node, ND_SUB);
            }
        } else {
            if (!type) {
                error_at(token->str, "型が指定されていません");
            }
            lvar = calloc(1, sizeof(LVar));
            lvar->next = locals;
            lvar->name = tok->str;
            lvar->len = tok->len;
            lvar->offset = locals->offset + 8;
            node->offset = lvar->offset;
            locals = lvar;
        }
        return node;
    }

    return new_node_num(expect_number());
}

Node *unary() {
    if (consume("++")) {
        Node *node = primary();
        Node *tmp = malloc(sizeof(Node));
        memcpy(tmp, node, sizeof(Node));
        return new_node(ND_ASSIGN, node, new_node(ND_ADD, tmp, new_node_num(1)));
    }
    if (consume("--")) {
        Node *node = primary();
        Node *tmp = malloc(sizeof(Node));
        memcpy(tmp, node, sizeof(Node));
        return new_node(ND_ASSIGN, node, new_node(ND_SUB, tmp, new_node_num(1)));
    }
    if (consume("+")) {
        return unary();
    }
    if (consume("-")) {
        return new_node(ND_SUB, new_node_num(0), unary());
    }
    if (consume("*")) {
        return new_node(ND_DEREF, unary(), NULL);
    }
    if (consume("&")) {
        return new_node(ND_ADDR, unary(), NULL);
    }
    return primary();
}

Node *mul() {
    Node *node = unary();

    for (;;) {
        if (consume("*")) {
            node = new_node(ND_MUL, node, unary());
        } else if (consume("/")) {
            node = new_node(ND_DIV, node, unary());
        } else {
            return node;
        }
    }
}

Node *add() {
    Node *node = mul();

    for (;;) {
        if (consume("+")) {
            node = new_node(ND_ADD, node, mul());
        } else if (consume("-")) {
            node = new_node(ND_SUB, node, mul());
        } else {
            return node;
        }
    }
}

Node *relational() {
    Node *node = add();

    for (;;) {
        if (consume("<")) {
            node = new_node(ND_LT, node, add());
        } else if (consume("<=")) {
            node = new_node(ND_LE, node, add());
        } else if (consume(">")) {
            node = new_node(ND_LT, add(), node);
        } else if (consume(">=")) {
            node = new_node(ND_LE, add(), node);
        } else {
            return node;
        }
    }
}

Node *equality() {
    Node *node = relational();

    for(;;) {
        if (consume("==")) {
            node = new_node(ND_EQ, node, relational());
        } else if (consume("!=")) {
            node = new_node(ND_NE, node, relational());
        } else {
            return node;
        }
    }
}

Node *code[100];

Node *assign() {
    Node *node = equality();

    if (consume("=")) {
        node = new_node(ND_ASSIGN, node, assign());
        return node;
    }else if (consume("+=")) {
        Node *tmp = malloc(sizeof(Node));
        memcpy(tmp, node, sizeof(Node));
        node = new_node(ND_ASSIGN, node, new_node(ND_ADD, tmp, assign()));
    } else if (consume("-=")) {
        Node *tmp = malloc(sizeof(Node));
        memcpy(tmp, node, sizeof(Node));
        node = new_node(ND_ASSIGN, node, new_node(ND_SUB, tmp, assign()));
    } else if (consume("*=")) {
        Node *tmp = malloc(sizeof(Node));
        memcpy(tmp, node, sizeof(Node));
        node = new_node(ND_ASSIGN, node, new_node(ND_MUL, tmp, assign()));
    } else if (consume("/=")) {
        Node *tmp = malloc(sizeof(Node));
        memcpy(tmp, node, sizeof(Node));
        node = new_node(ND_ASSIGN, node, new_node(ND_DIV, tmp, assign()));
    } else if (consume("=")) {
        node = new_node(ND_ASSIGN, node, assign());
    }
    return node;
}

Node *expr() { return assign(); }

Node *stmt() {
    Node *node;
    if (consume("return")) {
        node = new_node(ND_RETURN, expr(), NULL);
    } else if (consume("if")) {
        node = new_node(ND_IF, NULL, NULL);
        expect("(");
        node->lhs = expr();
        expect(")");
        node->rhs = new_node(ND_NOELSE, stmt(), NULL);
        if (consume("else")) {
            node->rhs->kind = ND_ELSE;
            node->rhs->rhs = stmt();
        }
        return node;
    } else if (consume("while")) {
        node = new_node(ND_WHILE, NULL, NULL);
        expect("(");
        node->lhs = expr();
        expect(")");
        node->rhs = stmt();
        return node;
    } else if (consume("for")) {
        node = new_node(ND_FOR, NULL, NULL);
        node->lhs = new_node(ND_EMPTY, NULL, NULL);
        node->rhs = new_node(ND_FORTEST, NULL, NULL);
        expect("(");
        if(!consume(";")) {
            node->lhs->lhs = expr();
            expect(";");
        } else {
            node->lhs->lhs = new_node(ND_EMPTY, NULL, NULL);
        }
        if(!consume(";")) {
            node->lhs->rhs = expr();
            expect(";");
        } else {
            node->lhs->rhs = new_node_num(1);
        }
        if(!consume(")")) {
            node->rhs->lhs = new_node(ND_STMT, expr(), NULL);
            expect(")");
        } else {
            node->rhs->lhs = new_node(ND_EMPTY, NULL, NULL);
        }
        node->rhs->rhs = stmt();
        return node;
    } else if (consume("{")) {
        node = new_node(ND_BLOCK, NULL, NULL);
        node->block = calloc(1, sizeof(NodeVec));
        NodeVec *nvec = node->block;
        while (!consume("}")) {
            nvec->node = stmt();
            nvec->next = calloc(1, sizeof(NodeVec));
            nvec = nvec->next;
        }
        return node;
    } else if (consume("continue")) {
        node = new_node(ND_CONTINUE, NULL, NULL);
    } else if (consume("break")) {
        node = new_node(ND_BREAK, NULL, NULL);
    } else {
        node = expr();
    }

    if (!consume(";")) {
        error_at(token->str, "';'ではないトークンです");
    }
    return new_node(ND_STMT, node, NULL);
}

Node *global() {
    Token *type = consume_type();
    if (!type) {
        error_at(token->str, "型が指定されていません");
    }
    Node *node;
    Token *tok = consume_ident();
    if (tok) {
        if (consume("(")) {
            locals = calloc(1, sizeof(LVar));
            node = func_declaration(tok, ND_FUNCDECL);

            if (consume(";")) {
                node->kind = ND_FUNCPROT;
            } else if (peek("{")) {
                node->lhs = stmt();
                node->fptr->locals = locals;
            } else {
                error_at(token->str, "関数の宣言にエラーが存在します");
            }
        } else {
            error_at(token->str, "実装上グローバル領域には関数しか存在できません");
        }
    } else {
        error_at(token->str, "グローバル領域にエラーが存在します");
    }
    return node;
}

void program() {
    int i = 0;
    while (!at_eof()) {
        code[i++] = global();
    }
    code[i] = NULL;
}
