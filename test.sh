#!/bin/bash
assert() {
    expected="$1"
    input="$2"

    ./9cc "$input" > tmp.s
    cc -o tmp tmp.s
    ./tmp
    actual="$?"

    if [ "$actual" = "$expected" ]; then
        echo "$input => $actual"
    else
        echo "$input => $expected expected, but got $actual"
        exit 1
    fi
}

assert 0 'int main(){return 0;}'
assert 42 'int main(){return 42;}'
assert 21 'int main(){return 5+20-4;}'
assert 41 'int main(){ return 12 + 34 - 5 ; }' 
assert 47 'int main(){return 5+6*7;}'
assert 15 'int main(){return 5*(9-6);}'
assert 4 'int main(){return (3+5)/2;}'
assert 10 'int main(){return -10+20;}'
assert 10 'int main(){return - -10;}'
assert 10 'int main(){return - - +10;}'

assert 0 'int main(){return 0==1;}'
assert 1 'int main(){return 42==42;}'
assert 1 'int main(){return 0!=1;}'
assert 0 'int main(){return 42!=42;}'

assert 1 'int main(){return 0<1;}'
assert 0 'int main(){return 1<1;}'
assert 0 'int main(){return 2<1;}'
assert 1 'int main(){return 0<=1;}'
assert 1 'int main(){return 1<=1;}'
assert 0 'int main(){return 2<=1;}'

assert 1 'int main(){return 1>0;}'
assert 0 'int main(){return 1>1;}'
assert 0 'int main(){return 1>2;}'
assert 1 'int main(){return 1>=0;}'
assert 1 'int main(){return 1>=1;}'
assert 0 'int main(){return 1>=2;}'

assert 101 'int main(){int a; return a=101;}'
assert 212 'int main(){int a; int b; return a=b=212;}'
assert 100 'int main(){int a; return a=100; int b; b=88;}'

assert 123 'int main(){int a; a=123; return a;}'
assert 101 'int main(){int a; a=2; if (1 == 1) a = 101; return a;}'
assert 101 'int main(){int a; a=6; if (a == 6) a = 101; return a;}'
assert 16 'int main(){int a; a=111; if (a != 6) a = 16; return a;}'
assert 111 'int main(){int a; a=111; if (a == 6) a = 12; return a;}'
assert 0 'int main(){if (1==1) return 0; else return 1;}'
assert 1 'int main(){if (1!=1) return 0; else return 1;}'
assert 10 'int main(){int var; var = 10; if (var == 10) return var; else return 11;}'
assert 11 'int main(){int var; var = 11; if (var == 10) return 0; else return var;}'

assert 10 '
int main() {
    int i;
    i = 0;
    while (i < 10)
        i = i + 1;
    return i;
}'
assert 255 '
int main() {
    int i;
    i = 0;
    while (i < 10)
        i = i + 1;
    while (i >= 0)
        i = i - 1;
    return i;
}'
assert 10 '
int main() {
    int i;
    i = 0;
    if (i == 0)
        while (i < 10)
            i = i + 1;
    return i;
}'
assert 0 '
int main() {
    int i;
    i = 0;
    if (i != 0)
        while (i < 10)
            i = i + 1;
    return i;
}'

assert 0 '
int main() {
    int i;
    for (i = 0;; i = i)
        return i;
}'
assert 100 '
int main() {
    int i;
    for (i = 0; i < 100; i = i + 1)
        1;
    return i;
}'
assert 5 '
int main() {
    int i;
    int counter;
    counter = 0;
    for (i = 0; i < 10; i = i + 2)
        counter = counter + 1;
    return counter;
}'

assert 0 '
int main() {
    int a;
    int b;
    int tmp;
    a = 10;
    b = 0;
    if (1 == 1) {
        tmp = a;
        a = b;
        b = tmp;
    }
    return a;
}'
assert 10 '
int main() {
    int a;
    int b;
    a = 10;
    b = 0;
    if (1 == 1) {
        b = a;
    }
    return b;
}'
assert 10 '
int main() {
    int a;
    int b;
    int tmp;
    a = 10;
    b = 0;
    if (1 == 2) {
        tmp = a;
        a = b;
        b = tmp;
    }
    return a;
}'
assert 0 '
int main() {
    int a;
    int b;
    int tmp;
    a = 10;
    b = 0;
    if (1 == 2) {
        tmp = a;
        a = b;
        b = tmp;
    }
    return b;
}'
assert 2 '
int main() {
    int a;
    int b;
    int c;
    int tmp;
    a = 10;
    b = 0;
    c = 2;
    if (1 == 2) {
        tmp = a;
        a = b;
        b = c;
        c = tmp;
    }
    return c;
}'
assert 0 '
int main() {
    int a;
    int b;
    int c;
    int tmp;
    a = 10;
    b = 0;
    c = 2;
    if (a != c) {
        tmp = a;
        a = b;
        b = c;
        c = tmp;
    }
    return a;
}'
assert 2 '
int main() {
    int a;
    int b;
    int c;
    int tmp;
    a = 10;
    b = 0;
    c = 2;
    if (a != c) {
        tmp = a;
        a = b;
        b = c;
        c = tmp;
    }
    return b;
}'
assert 10 '
int main() {
    int a;
    int b;
    int c;
    int tmp;
    a = 10;
    b = 0;
    c = 2;
    if (a != c) {
        tmp = a;
        a = b;
        b = c;
        c = tmp;
    }
    return c;
}'
assert 101 '
int main() {
    int i;
    for (i = 0; i < 1000; i = i + 1) {
        if (i > 100) {
            return i;
        }
    }
    return 2;
}'
assert 12 '
int main() {
    int i;
    for (i = 0; i < 1000; i = i + 1) {
        if (i == 10) {
            return 12;
        }
        if (i > 100) {
            return i;
        }
    }
    return 2;
}'
assert 255 '
int main() {
    int i;
    for (i = 0; i < 1000; i = i + 1) {
        if (i == 10) {
            return 12;
            return 11;
        } else if (i > 100) {
            return i;
            return i + 3;
        } else if (i == 8) {
            return i;
            return i + i;
        }
        while (i < 254) {
            i = i + 1;
        }
    }
    return i;
}'
assert 10 '
int main() {
    int left;
    int right;
    left = 12;
    right = 22;
    if (left == right) {
        return left;
        return left + right;
    } else if (left > right) {
        return right;
    } else if (left < right) {
        return right - left;
    }
    return 1;
}'
assert 1 '
int main() {
    if (1 == 2) {
        return 123;
        return 8;
    }
    return 1;
}'

assert 3 '
int main() {
    return 1 + 2;
}'
assert 30 '
int add(int a, int b) {
    return a + b;
}

int main() {
    return add(10, 20);
}'
assert 55 '
int fib(int a) {
    if (a == 1) {
        return 1;
    } else if (a == 0) {
        return 0;
    }
    return fib(a - 1) + fib(a - 2);
}

int main() {
    return fib(10);
}'
assert 89 '
int main() {
    int a;
    int b;
    a = 0;
    b = 1;
    int c;
    int i;
    for (i = 0; i < 10; i = i + 1) {
        c = a + b;
        a = b;
        b = c;
    }
    return b;
}'

assert 10 '
int main() {
    int x;
    int y;
    x = 10;
    y = &x;
    return *y;
}'
assert 101 '
int main() {
    int x;
    int y;
    int z;
    x = 101;
    y = &x;
    z = &y + 8;
    return *z;
}'

assert 128 '
int main() {
    for (int i = 0; i < 10000000; i += 1) {}
    return i;
}'
assert 8 '
int func(int start, int end) {
    int result = 0;
    for (; start < end; start += 1) {
        result += 1;
    }
    return result;
}

int main() {
    int exitcode = func(2000000, 10000008);
    return exitcode;
}'
assert 16 '
int main() {
    int result = 0;
    for (int i = 0; i < 100; i += 1) {
        for (int j = 0; j < 100; j += 1) {
            result += 1;
        }
    }
    return result;
}'

assert 16 '
int main() {
    int result = 0;
    for (int i = 0; i < 100; ++i) {
        for (int j = 0; j < 100; ++j) {
            ++result;
        }
    }
    return result;
}'
assert 1 '
int main() {
    int result = 0;
    return ++result;
}'
assert 255 '
int main() {
    int result = 0;
    return --result;
}'
assert 255 '
int main() {
    int result = 100000;
    for (int i = 100000; i >= 0; --i){--result;}
    return result;
}'

assert 16 '
int main() {
    int result = 0;
    for (int i = 0; i < 100; i++) {
        for (int j = 0; j < 100; j++) {
            result++;
        }
    }
    return result;
}'
assert 123 '
int main() {
    int result = 123;
    return result++;
}'
assert 124 '
int main() {
    int result = 123;
    result++;
    return result++;
}'
assert 11 '
int main() {
    int result = 11;
    return result--;
}'
assert 10 '
int main() {
    int result = 11;
    result--;
    return result--;
}'
assert 255 '
int main() {
    int result = 100000;
    for (int i = 100000; i >= 0; i--){result--;}
    return result;
}'
assert 255 '
int main() {
    int result = 100000;
    for (int i = 100000; i >= 0; i--){result--;}
    return result++;
}'
assert 0 '
int main() {
    int result = 100000;
    for (int i = 100000; i >= 0; i--){result--;}
    return ++result;
}'

assert 11 '
int main() {
    int i = 0;
    while (1) {
        if (i++ == 10) {
            break;
        }
    }
    return i;
}'
assert 102 '
int main() {
    int i = 0;
    while (1) {
        if (i++ <= 100) {
            continue;
        }
        break;
    }
    return i;
}'
assert 108 '
int main() {
    int a = 0;
    for (int i = 0; i < 100; ++i) {
        for (int j = 0; j < 100; ++j) {
            if (j >= 30) {
                break;
            }
            a += 1;
        }
        if (i < 10) {
            continue;
        }
        a += 2;
    }
    return a;
}'
assert 82 '
int main() {
    int a = 0;
    while (1) {
        a += 3;
        if (a > 100) {
            break;
        }
    }

    for (int i = 0; i < 100; ++i) {
        for (int j = 0; j < 100; ++j) {
            if (j > 50) {
                continue;
            }
            a += 2;
        }
        a -= 10;
        if (i >= 60) {
            break;
        }
    }
    return a;
}'

echo OK
