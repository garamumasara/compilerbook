#include "9cc.h"

char *labelVec[10000];
char label[100];
int pos = 0;
int loop = 0;

void next_string() {
    if (label[pos] < 'Z') {
        ++label[pos];
        return;
    }

    while (pos >= 0 && label[pos] >= 'Z') {
        --pos;
    }

    if (pos >= 0) {
        ++label[pos++];
        for (; label[pos] != '\0'; ++pos) {
            label[pos] = 'A';
        }
        --pos;
    } else {
        for (pos = 0; label[pos] != '\0'; ++pos) {
            label[pos] = 'A';
        }
        if (pos == 100) {
            error("ラベルのパターンが上限に達しました");
        }
        label[pos] = 'A';
        label[pos + 1] = '\0';
    }
}

void gen_label(char local_lbl[100]) {
    strcpy(local_lbl, label);
    next_string();
}

void gen_lval(Node *node) {
    if (node->kind != ND_LVAR) {
        error("代入の左辺値が変数ではありません");
    }

    printf("  mov   rax, rbp\n");
    printf("  sub   rax, %d\n", node->offset);
    printf("  push  rax\n");
}


void gen(Node *node) {
    char local_lbl[100];
    char *registers[] = {"rdi", "rsi", "rdx", "rcx", "r8d", "r9d", NULL};
    NodeVec *args;
    switch (node->kind) {
    case ND_NUM:
        printf("  push  %d\n", node->val);
        return;
    case ND_LVAR:
        gen_lval(node);
        printf("  pop   rax\n");
        printf("  mov   rax, [rax]\n");
        printf("  push  rax\n");
        return;
    case ND_ADDR:
        gen_lval(node->lhs);
        return;
    case ND_DEREF:
        gen(node->lhs);
        printf("  pop   rax\n");
        printf("  mov   rax, [rax]\n");
        printf("  push  rax\n");
        return;
    case ND_ASSIGN:
        gen_lval(node->lhs);
        gen(node->rhs);

        printf("  pop   rdi\n");
        printf("  pop   rax\n");
        printf("  mov   [rax], rdi\n");
        printf("  push  rdi\n");
        return;
    case ND_RETURN:
        gen(node->lhs);

        printf("  pop   rax\n");
        printf("  mov   rsp, rbp\n");
        printf("  pop   rbp\n");
        printf("  ret\n");
        return;
    case ND_IF:
        gen(node->lhs);

        gen_label(local_lbl);

        printf("  pop   rax\n");
        printf("  cmp   rax, 0\n");

        switch (node->rhs->kind) {
        case ND_NOELSE:
            printf("  je    .Lend%s\n", local_lbl);
            gen(node->rhs->lhs);
            printf(".Lend%s:\n", local_lbl);
            break;
        case ND_ELSE:
            printf("  je    .Lelse%s\n", local_lbl);
            gen(node->rhs->lhs);
            printf("  jmp   .Lend%s\n", local_lbl);
            printf(".Lelse%s:\n", local_lbl);
            gen(node->rhs->rhs);
            printf(".Lend%s:\n", local_lbl);
            break;
        default:
            error("ifステートメントの後に構文エラーが存在します");
        }
        return;
    case ND_WHILE:
        ++loop;
        gen_label(local_lbl);
        labelVec[loop] = malloc(100);
        strcpy(labelVec[loop], local_lbl);

        printf(".Lbegin%s:\n", local_lbl);
        printf(".Lcont%s:\n", local_lbl);
        gen(node->lhs);
        printf("  pop   rax\n");
        printf("  cmp   rax, 0\n");
        printf("  je    .Lend%s\n", local_lbl);
        gen(node->rhs);
        printf("  jmp   .Lbegin%s\n", local_lbl);
        printf(".Lend%s:\n", local_lbl);
        free(labelVec[loop]);
        --loop;
        return;
    case ND_FOR:
        ++loop;
        gen_label(local_lbl);
        labelVec[loop] = malloc(100);
        strcpy(labelVec[loop], local_lbl);

        gen(node->lhs->lhs);
        printf("  pop   rax\n");
        printf(".Lbegin%s:\n", local_lbl);
        gen(node->lhs->rhs);
        printf("  pop   rax\n");
        printf("  cmp   rax, 0\n");
        printf("  je    .Lend%s\n", local_lbl);
        gen(node->rhs->rhs);
        printf(".Lcont%s:\n", local_lbl);
        gen(node->rhs->lhs);
        printf("  jmp   .Lbegin%s\n", local_lbl);
        printf(".Lend%s:\n", local_lbl);
        free(labelVec[loop]);
        --loop;
        return;
    case ND_CONTINUE:
        if (loop == 0) {
            error("ループ外でcontinueが使用されています");
        }
        printf("  jmp   .Lcont%s\n", labelVec[loop]);
        printf("  push  rax\n");
        return;
    case ND_BREAK:
        if (loop == 0) {
            error("ループ外でbreakが使用されています");
        }
        printf("  jmp   .Lend%s\n", labelVec[loop]);
        printf("  push  rax\n");
        return;
    case ND_EMPTY:
        printf("  push  rax\n");
        return;
    case ND_BLOCK:
        for (NodeVec *nvec = node->block; nvec->next != NULL; nvec = nvec->next) {
            gen(nvec->node);
        }
        return;
    case ND_FUNCPROT:
        return;
    case ND_FUNCCALL:
        printf("  mov   rax, rsp\n");
        printf("  mov   rdi, 16\n");
        printf("  cqo\n");
        printf("  idiv  rdi\n");
        printf("  sub   rsp, rdx\n");
        printf("  push  rdx\n");
        args = node->fptr->args;
        for (int i = 0; registers[i] != NULL && args->next != NULL; ++i, args = args->next) {
            gen(args->node);
            printf("  pop   %s\n", registers[i]);
        }
        if (args->next != NULL) {
            error("引数が6つ以上存在します");
        }
        if (node->fptr->len >= 100) {
            error("関数の名前が長すぎます");
        }
        memcpy(local_lbl, node->fptr->name, node->fptr->len);
        local_lbl[node->fptr->len] = '\0';
        printf("  call  %s\n", local_lbl);
        printf("  pop   rdx\n");
        printf("  add   rsp, rdx\n");
        printf("  push  rax\n");
        return;
    case ND_FUNCDECL:
        if (node->fptr->len >= 100) {
            error("関数の名前が長すぎます");
        }
        memcpy(local_lbl, node->fptr->name, node->fptr->len);
        local_lbl[node->fptr->len] = '\0';
        printf("%s:\n", local_lbl);
        printf("  push  rbp\n");
        printf("  mov   rbp, rsp\n");
        printf("  sub   rsp, %d\n", node->fptr->locals->offset);
        args = node->fptr->args;
        for (int i = 0; registers[i] != NULL && args->next != NULL; ++i, args = args->next) {
            printf("  mov   [rbp - %d], %s\n", args->node->offset, registers[i]);
        }
        if (args->next != NULL) {
            error("引数が6つ以上存在します");
        }
        gen(node->lhs);
        printf("  mov   rsp, rbp\n");
        printf("  pop   rbp\n");
        printf("  ret\n");
        return;
    case ND_STMT:
        gen(node->lhs);
        printf("  pop   rax\n");
        return;
    }

    gen(node->lhs);
    gen(node->rhs);

    printf("  pop   rdi\n");
    printf("  pop   rax\n");

    switch (node->kind) {
    case ND_ADD:
        printf("  add   rax, rdi\n");
        break;
    case ND_SUB:
        printf("  sub   rax, rdi\n");
        break;
    case ND_MUL:
        printf("  imul   rax, rdi\n");
        break;
    case ND_DIV:
        printf("  cqo\n");
        printf("  idiv  rdi\n");
        break;
    case ND_EQ:
        printf("  cmp   rax, rdi\n");
        printf("  sete  al\n");
        printf("  movzb rax, al\n");
        break;
    case ND_NE:
        printf("  cmp   rax, rdi\n");
        printf("  setne al\n");
        printf("  movzb rax, al\n");
        break;
    case ND_LT:
        printf("  cmp   rax, rdi\n");
        printf("  setl  al\n");
        printf("  movzb rax, al\n");
        break;
    case ND_LE:
        printf("  cmp   rax, rdi\n");
        printf("  setle al\n");
        printf("  movzb rax, al\n");
        break;
    }

    printf("  push  rax\n");
}
