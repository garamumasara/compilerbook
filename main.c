#include "9cc.h"

int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "引数の個数が正しくありません\n");
        return 1;
    }

    user_input = argv[1];
    token = tokenize(user_input);

    // for (Token *tk = token; tk->kind != TK_EOF; tk = tk->next) {
    //     if (tk->len) {
    //         for (int i = 0; i < tk->len; ++i) {
    //             printf("%c", tk->str[i]);
    //         }
    //     } else {
    //         printf("val: %d", tk->val);
    //     }
    //     printf("\n");
    // }

    funcs = calloc(1, sizeof(Funcs));
    label[0] = 'A';
    label[1] = '\0';
    program();

    printf(".intel_syntax noprefix\n");
    for (Funcs *f = funcs; f; f = f->next) {
        if (f->len == 4 && !memcmp(f->name, "main", 4)) {
            printf(".globl main\n");
            break;
        }
    }

    for (int i = 0; code[i]; ++i) {
        gen(code[i]);
    }

    return 0;
}
