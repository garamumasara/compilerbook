CFLAGS=-std=c11 -g -static
SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)

COMPILER=9cc
TEMP=tmp

$(COMPILER): $(OBJS)
	$(CC) -o $@ $(OBJS) $(LDFLAGS)

$(OBJS): $(COMPILER).h

test: $(COMPILER)
	./test.sh

CODE=return\ 0\;
run: $(COMPILER)
	./$< '$(CODE)' > $(TEMP).s
	$(CC) -o $(TEMP) $(TEMP).s && ./$(TEMP); echo 'return code: '"$$?"

clean:
	rm -f $(COMPILER) *.o *~ $(TEMP)*

.PHONY: test clean run run-%
